import cherrypy
import mako.template
import mako.lookup
import os

from lib.tool import web
from conf.system_config import tmpl_admin_lookup
from lib.plugin.auth import require, is_admin
from webapp.common import AuthController
from webapp.admin.message import AdminMessageController
from lib.model.message import AdminMessage

__all__ = ["AdminController"]


class AdminController(object):

    _cp_config = {
        "tools.auth.on": True,
        "error_page.404": web.handle_404_error
        }

    auth = AuthController

    def __init__(self):
        cherrypy.tree.mount(
            AdminMessageController(),
            "/admin/message",
            "conf/app.cfg"
            )

    @cherrypy.expose
    @require(is_admin())
    def index(self):
        tmpl = mako.template.Template(
            filename="template/admin/dashboard/index.html",
            lookup=tmpl_admin_lookup
            )
        return tmpl.render(
            current_user=web.get_current_user(),
            unread_messages=AdminMessage.all_unread()
            )
