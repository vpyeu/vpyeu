import cherrypy
import mako.template
import mako.lookup
import os

from lib.tool import web
from conf.system_config import tmpl_admin_lookup
from lib.plugin.auth import require, is_admin
from webapp.common import AuthController
from lib.model.message import AdminMessage
from lib.tool import mail

__all__ = ["AdminMessageController"]


class AdminMessageController(object):

    _cp_config = {
        "tools.auth.on": True,
        "error_page.404": web.handle_404_error
        }

    auth = AuthController

    @cherrypy.expose
    @require(is_admin())
    def index(self):
        tmpl = mako.template.Template(
            filename="template/admin/message/index.html",
            lookup=tmpl_admin_lookup
            )
        return tmpl.render(
            current_user=web.get_current_user(),
            unread_messages=AdminMessage.all_unread(),
            messages=AdminMessage.all()
            )

    @cherrypy.expose
    @require(is_admin())
    def view(self, message_id=None):
        message = AdminMessage.get_by("message_id", message_id)
        if message:
            message.set_read()
        tmpl = mako.template.Template(
            filename="template/admin/message/view.html",
            lookup=tmpl_admin_lookup
            )
        return tmpl.render(
            current_user=web.get_current_user(),
            unread_messages=AdminMessage.all_unread(),
            message=message
            )

    @cherrypy.expose
    @require(is_admin())
    @cherrypy.tools.json_out()
    def j_feedback(self, message_id, content):
        message = AdminMessage.get_by("message_id", message_id)
        if not message:
            return {"status": "error"}
        mail.admin_send_answer(
            receiver=message.sender_email,
            receiver_name=message.sender,
            content=content,
            origin=message.content
            )
        return {"status": "success"}
