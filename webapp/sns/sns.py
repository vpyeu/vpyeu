import cherrypy
import mako.template
import mako.lookup
import os

from webapp.sns.post import PostController
from webapp.sns.comment import CommentController
from lib.model.post import Post
from lib.tool import web
from conf.system_config import tmpl_lookup

__all__ = ["Sns"]


class Sns(object):

    _cp_config = {
        "error_page.404": web.handle_404_error
        }

    def __init__(self):
        cherrypy.tree.mount(
            PostController(),
            "/sns/post",
            "conf/app.cfg"
            )
        cherrypy.tree.mount(
            CommentController(),
            "/sns/comment",
            "conf/app.cfg"
            )

    @cherrypy.expose
    def index(self):
        posts = Post.all()
        current_user = web.get_current_user()
        tmpl = mako.template.Template(
            filename="template/sns/top/index.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(
                current_user=current_user,
                posts=posts
                )
