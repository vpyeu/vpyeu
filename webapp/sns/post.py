import cherrypy
import mako.template
import mako.lookup

from lib.plugin.auth import require
from webapp.common import AuthController
from lib.tool import web
from lib.model.post import Post

__all__ = ["PostController"]


class PostController(object):

    _cp_config = {
        "tools.auth.on": True,
        "error_page.404": web.handle_404_error
        }

    auth = AuthController

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect("/sns/")

    @cherrypy.expose
    @cherrypy.tools.json_out()
    @require()
    def j_create(self, content=None):
        content = " ".join(content.split())
        if cherrypy.request.method == "POST" and content:
            return self._create_post(content)
        return {"status": "error"}

    def _create_post(self, content=None):
        current_user = web.get_current_user()
        status = Post.create(content, current_user)
        if status:
            return {"status": "success"}
        else:
            return {"status": "error"}
