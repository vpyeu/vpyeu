import cherrypy
import json
import mako.template

from lib.plugin.auth import require
from webapp.common import AuthController
from lib.tool import web
from lib.model.post import Post
from lib.model.comment import Comment
from conf.system_config import tmpl_lookup

__all__ = ["CommentController"]


class CommentController(object):

    _cp_config = {
        "tools.auth.on": True,
        "error_page.404": web.handle_404_error
        }

    auth = AuthController

    @cherrypy.expose
    @require()
    def comment(self, post_id=None, content=None):
        if cherrypy.request.method == "GET":
            return self._get_comment_form(post_id)
        elif cherrypy.request.method == "POST":
            content = " ".join(content.split())
            if not content:
                return json.dumps({"status": "error"})
            status = self._do_comment(post_id, content)
            if status:
                return json.dumps({"status": "success"})
            else:
                return json.dumps({"status": "error"})

    def _do_comment(self, post_id=None, content=None):
        if not post_id or not content:
            return False
        return Comment.create(
            "Post",
            post_id,
            content,
            web.get_current_user()
            )

    def _get_comment_form(self, post_id=None):
        post = Post.get_by("post_id", post_id)
        if not post:
            return
        tmpl = mako.template.Template(
            filename="template/sns/comments/comment.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(
            current_user=web.get_current_user(),
            post_id=post_id,
            post=post,
            comments=post[0].get_comments()
            )
