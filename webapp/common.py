import cherrypy
import mako
import mako.template
import mako.lookup

from lib.plugin import auth
from lib.tool import web
from lib.plugin.auth import require, member_of, name_is
from lib.model.user import User
from lib.model.message import AdminMessage
from conf.system_config import SESSION_KEY, tmpl_lookup

__all__ = ["Common", "AuthController", "UserController"]


class Common(object):

    _cp_config = {
        "error_page.404": web.handle_404_error
        }

    @cherrypy.expose
    def index(self):
        tmpl = mako.template.Template(
            filename="template/common/index.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(
            current_user=web.get_current_user()
            )

    @cherrypy.expose
    def about(self):
        tmpl = mako.template.Template(
            filename="template/common/about.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(
            current_user=web.get_current_user()
            )

    @cherrypy.expose
    def privacy(self):
        tmpl = mako.template.Template(
            filename="template/common/privacy.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(
            current_user=web.get_current_user()
            )

    @cherrypy.expose
    def contact(self):
        tmpl = mako.template.Template(
            filename="template/common/contact.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(
            current_user=web.get_current_user()
            )

    @cherrypy.expose
    def activate(self, email=None, key=None):
        if web.get_current_user():
            raise cherrypy.HTTPRedirect("/")
        status = None
        if email and key:
            status = self._do_activate(email, key)
            if not status:
                return self._get_active_page("activated")
        return self._get_active_page("activate", status)

    def _get_active_page(self, status="activate", msg=None):
        tmpl = mako.template.Template(
            filename="template/common/%s.html" % status,
            lookup=tmpl_lookup
            )
        return tmpl.render(
            current_user=web.get_current_user(),
            message=msg
            )

    def _do_activate(self, email=None, key=None):
        user = User.get_by("email", email)
        if user:
            return user.activate(key)
        return False

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def j_resend_key(self, email=None):
        user = User.get_by("email", email)
        if not user:
            return {"status": "error"}
        status = user.generate_key()
        return {"status": "success" if status else "error"}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def j_contact(self, sender=None, sender_email=None, content=None):
        if not sender or not sender_email or not content:
            return {"status": "error"}
        AdminMessage.create(content, sender, sender_email)
        return {"status": "success"}

class AuthController(object):

    _cp_config = {
        "error_page.404": web.handle_404_error
        }

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def login(self, username=None, password=None, from_page="/"):
        if web.get_current_user():
            raise cherrypy.HTTPRedirect(from_page)
        if not web.is_xhr():
            raise cherrypy.HTTPRedirect(from_page)
        if cherrypy.request.method == "POST":
            error_msg = auth.check_credentials(username, password)
            if error_msg:
                return {"status": error_msg}
            else:
                cherrypy.session.regenerate()
                cherrypy.session[SESSION_KEY] = cherrypy.request.login =\
                    username
                return {"status": "success"}
        return "You have to login"

    @cherrypy.expose
    def logout(self, from_page="/"):
        sess = cherrypy.session
        username = sess.get(SESSION_KEY, None)
        sess[SESSION_KEY] = None
        if username:
            cherrypy.request.login = None
        raise cherrypy.HTTPRedirect(from_page or "/")

class UserController(object):

    _cp_config = {
        "tools.auth.on": True,
        "error_page.404": web.handle_404_error
        }

    auth = AuthController

    @cherrypy.expose
    def index(self):
        current_user = web.get_current_user()
        if current_user:
            raise cherrypy.HTTPRedirect("/user/view/%s" % current_user.name)
        raise cherrypy.HTTPRedirect("/user/register")

    @cherrypy.expose
    @require()
    def view(self, username=None):
        current_user = web.get_current_user()
        if not username:
            raise cherrypy.HTTPRedirect("/user/view/%s" % current_user.name)
        user = User.get_by("name", username)
        if not user:
            raise cherrypy.HTTPError(
                status="404 Not Found",
                message="User '%s' not found" % username
                )
        tmpl = mako.template.Template(
            filename="template/user/view/profile.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(
            current_user=current_user,
            user=user
            )

    @cherrypy.expose
    @require()
    def avatar(self, username=None):
        current_user = web.get_current_user()
        if not username:
            raise cherrypy.HTTPRedirect(
                "/user/avatar/%s" % current_user.name
                )
        user = User.get_by("name", username)
        tmpl = mako.template.Template(
            filename="template/user/view/avatar.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(
            current_user=current_user,
            user=user
            )

    @cherrypy.expose
    @require()
    def edit(self, name=None, fullname=None, email=None, password=None,
             pr=None):
        if not name:
            raise cherrypy.HTTPRedirect(
                "/user/edit/%s" % web.get_current_user().name
                )
        if cherrypy.request.method == "GET":
            return self._get_edit_form(name)
        elif cherrypy.request.method == "POST":
            self._edit_user(name, fullname, email, password, pr)
        raise cherrypy.HTTPRedirect("/user/view/%s" % name)

    @cherrypy.expose
    def register(self, name=None, fullname=None, email=None, password=None,
                 pr=None):
        current_user = web.get_current_user()
        status = False
        if current_user:
            raise cherrypy.HTTPRedirect("/user/view/%s" % current_user.name)
        if cherrypy.request.method == "GET":
            return self._get_register_form()
        elif cherrypy.request.method == "POST":
            status = self._do_register(name, fullname, email, password, pr)
        if status:
            raise cherrypy.HTTPRedirect("/")

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def j_is_valid(self, attr_name="name", value=None, accept=None):
        if User.is_valid_attr(attr_name, value, accept):
            result = {"status": "valid"}
        else:
            result = {"status": "invaid"}
        return result

    def _get_edit_form(self, name=None):
        tmpl = mako.template.Template(
            filename="template/user/edit/form.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(current_user=web.get_current_user())

    def _edit_user(self, name=None, fullname=None, email=None, password=None,
                   pr=None):
        current_user = web.get_current_user()
        current_user.update(name, fullname, email, password, pr)

    def _get_register_form(self):
        tmpl = mako.template.Template(
            filename="template/user/register/form.html",
            lookup=tmpl_lookup
            )
        return tmpl.render()

    def _do_register(self, name=None, fullname=None, email=None,
                     password=None, pr=None):
        if not name or not email or not password:
            return False
        return User.create(name, fullname, email, password, pr)
