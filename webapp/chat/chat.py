import random
import cherrypy
from ws4py.server.cherrypyserver import WebSocketPlugin, WebSocketTool
from ws4py.websocket import WebSocket
from ws4py.messaging import TextMessage
import mako.template
import mako.lookup

from conf.system_config import tmpl_lookup
from lib.tool import web
from lib.plugin.auth import require
from webapp.common import AuthController

class Chat(object):

    _cp_config = {
        "tools.auth.on": True
        }

    auth = AuthController

    def __init__(self):
        WebSocketPlugin(cherrypy.engine).subscribe()
        cherrypy.tools.websocket = WebSocketTool()

    @cherrypy.expose
    @require()
    def index(self):
        tmpl = mako.template.Template(
            filename="template/chat/index.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(
            current_user=web.get_current_user()
            )

    @cherrypy.expose
    def ws(self):
        cherrypy.log("Handler created: %s" % (
                repr(cherrypy.request.ws_handler))
                     )
