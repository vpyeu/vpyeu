import cherrypy
import mako.template
import mako.lookup
import os
from datetime import datetime
from PIL import Image

from lib.tool import web
from conf.system_config import tmpl_lookup


__all__ = ["Gallery"]


class Gallery(object):

    _cp_config = {
        "error_page.404": web.handle_404_error
        }

    @cherrypy.expose
    def index(self):
        photos = [f for f in os.listdir("public/images/gallery/")
                  if (f.endswith(".jpg") or
                      f.endswith(".JPG") or
                      f.endswith(".jpeg"))]
        tmpl = mako.template.Template(
            filename="template/gallery/index.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(
                current_user=web.get_current_user(),
                photos=photos
                )

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def upload(self, files=None):
        if files:
            filename = datetime.utcnow().strftime("%Y%m%d%H%M%S")
            tmp_file = "public/images/gallery/" + filename + ".orig"
            thumb_file = "public/images/gallery/thumbnails/%s.jpg" % filename
            file_up = "public/images/gallery/" + filename + ".jpg"
            with open(tmp_file, "wb") as f:
                f.write(files.file.read())
            im = Image.open(tmp_file)
            im.thumbnail((1080, 720), Image.ANTIALIAS)
            if im.mode != "RGB":
                im = im.convert("RGB")
            im.save(file_up, "jpeg")
            im.thumbnail((240, 144), Image.ANTIALIAS)
            im.save(thumb_file, "jpeg")
            os.remove(tmp_file)
            return {"files": [{
                        "name": filename + ".jpg",
                        "size": "640x310",
                        "thumbnailUrl": \
                            "/static/images/gallery/thumbnails/%s.jpg" %(
                                        filename)
                        }]
                    }
