import cherrypy
import mako.template
import mako.lookup

from lib.tool import web
from webapp.blog.entry import EntryController
from webapp.blog.author import AuthorController
from conf.system_config import tmpl_lookup

__all__ = ["Blog"]


class Blog(object):

    _cp_config = {
        "error_page.404": web.handle_404_error
        }

    def __init__(self):
        cherrypy.tree.mount(
            EntryController(),
            "/blog/entry",
            "conf/app.cfg"
            )

        cherrypy.tree.mount(
            AuthorController(),
            "/blog/author",
            "conf/app.cfg"
            )

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect("/blog/entry")
