import cherrypy
import mako.template
import mako.lookup

from lib.plugin.auth import require, is_admin
from webapp.common import AuthController
from lib.tool import web
from lib.model.entry import Entry
from conf.system_config import tmpl_lookup

__all__ = ["EntryController"]


class EntryController(object):

    _cp_config = {
        "tools.auth.on": True,
        "error_page.404": web.handle_404_error
        }

    auth = AuthController

    @cherrypy.expose
    def index(self):
        tmpl = mako.template.Template(
            filename="template/blog/entry/index.html",
            lookup=tmpl_lookup
            )
        current_user = web.get_current_user()
        entries = Entry.get_for(current_user)
        return tmpl.render(
            current_user=current_user,
            entries=entries
            )

    @cherrypy.expose
    @require(is_admin())
    def write(self, entry_id=None):
        tmpl = mako.template.Template(
            filename="template/blog/entry/write.html",
            lookup=tmpl_lookup
            )
        entry = Entry.get_by("entry_id", entry_id)[0] if entry_id else Entry()
        return tmpl.render(
            current_user=web.get_current_user(),
            entry=entry
            )

    @cherrypy.expose
    @cherrypy.tools.json_out()
    @require(is_admin())
    def j_save(self, entry_id=None, headline=None, body_text=None):
        if cherrypy.request.method != "POST":
            return {"status": "error"}
        entry = self._do_save(entry_id, headline, body_text)
        return {"status": "success", "entry_id": entry.entry_id}

    def _do_save(self, entry_id=None, headline=None, body_text=None):
        entry = Entry.get_by("entry_id", entry_id)[0] if entry_id else None
        if entry:
            entry.update(headline, body_text)
        else:
            entry = Entry.create(headline, body_text)
        return entry

    @cherrypy.expose
    @cherrypy.tools.json_out()
    @require(is_admin())
    def j_publish(self, entry_id=None, headline=None, body_text=None):
        if cherrypy.request.method != "POST":
            return {"status": "error"}
        entry = self._do_publish(entry_id, headline, body_text)
        return {"status": "success", "entry_id": entry.entry_id}

    def _do_publish(self, entry_id=None, headline=None, body_text=None):
        entry = self._do_save(entry_id, headline, body_text)
        entry.publish()
        return entry

    @cherrypy.expose
    def all(self):
        return "ALL"

    @cherrypy.expose
    def view(self, entry_id=None):
        entry = Entry.get_by("entry_id", entry_id)
        if not entry:
            raise cherrypy.HTTPError(
                status="404 Not Found",
                message="Entry '#%s' not found" % entry_id
                )
        tmpl = mako.template.Template(
            filename="template/blog/entry/view.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(
            current_user=web.get_current_user(),
            entry=entry
            )
