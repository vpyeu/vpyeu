import cherrypy
import mako.template
import mako.lookup

from lib.tool import web
from lib.model.entry import Entry
from lib.model.user import User
from conf.system_config import tmpl_lookup

__all__ = ["AuthorController"]

class AuthorController(object):

    _cp_config = {
        "error_page.404": web.handle_404_error
        }

    @cherrypy.expose
    def index(self):
        tmpl = mako.template.Template(
            filename="template/blog/author/index.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(
            current_user=web.get_current_user(),
            authors=User.get_admin()
            )

    @cherrypy.expose
    def view(self, author=None):
        author = User.get_by("name", author)
        if not author:
            raise cherrypy.NotFound()
        tmpl = mako.template.Template(
            filename="template/blog/author/view.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(
            current_user=web.get_current_user(),
            author=author,
            entries=author.get_entries()
            )
