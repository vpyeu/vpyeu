import cherrypy
import mako.template
import mako.lookup

from lib.tool import web
from conf.system_config import tmpl_lookup

__all__ = ["Game"]


class Game(object):

    _cp_config = {
        "error_page.404": web.handle_404_error
        }

    @cherrypy.expose
    def index(self):
        tmpl = mako.template.Template(
            filename="template/game/top/index.html",
            lookup=tmpl_lookup
            )
        return tmpl.render(
                current_user=web.get_current_user()
                )

    @cherrypy.expose
    def play(self, name="tetris"):
        tmpl = mako.template.Template(
            filename="template/game/game/%s.html" %name,
            lookup=tmpl_lookup
            )
        return tmpl.render(
                current_user=web.get_current_user()
                )
