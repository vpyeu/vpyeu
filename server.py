#!/usr/bin/env python3

import os
import cherrypy
import sys

import webapp
import webapp.common
import webapp.sns.sns
import webapp.blog.blog
import webapp.game.game
import webapp.gallery.gallery
import webapp.chat.chat
import webapp.chat.handler
import webapp.admin.admin
from lib.tool import db
from lib.plugin import db_plug


class Server(object):

    def __init__(self):
        self._set_basic_config()
        self._setup()
        self._setup_db()
        self._add_app()

    def _set_basic_config(self):
         self.base_dir = os.path.dirname(os.path.abspath(__file__))
         self.conf_path = os.path.join(self.base_dir, "conf")
         log_dir = os.path.join(self.base_dir, "logs")
         if not os.path.exists(log_dir):
             os.mkdir(log_dir)
         session_dir = os.path.join(self.base_dir, "data")
         if not os.path.exists(session_dir):
             os.mkdir(session_dir)
         gallery_dir = os.path.join(self.base_dir, "public/images/gallery")
         if not os.path.exists(gallery_dir):
             os.mkdir(gallery_dir)
         gallery_thumb_dir = os.path.join(
             self.base_dir,
             "public/images/gallery/thumbnails"
             )
         if not os.path.exists(gallery_thumb_dir):
             os.mkdir(gallery_thumb_dir)


    def _setup(self):
        # Update the global settings for the HTTP server and engine
        cherrypy.config.update(os.path.join(self.conf_path, "server.cfg"))

    def _setup_db(self):
        engine = cherrypy.engine
        cherrypy.tools.db = db.SATool()
        engine.db = db_plug.SAEnginePlugin(engine)
        engine.db.subscribe()

    def _add_app(self):
        cherrypy.tree.mount(
            webapp.common.Common(),
            "/",
            os.path.join(self.conf_path, "app.cfg")
            )

        cherrypy.tree.mount(
            webapp.common.UserController(),
            "/user/",
            os.path.join(self.conf_path, "app.cfg")
            )

        cherrypy.tree.mount(
            webapp.common.AuthController(),
            "/auth/",
            os.path.join(self.conf_path, "app.cfg")
            )

        cherrypy.tree.mount(
            webapp.sns.sns.Sns(),
            "/sns/",
            os.path.join(self.conf_path, "app.cfg")
            )

        cherrypy.tree.mount(
            webapp.blog.blog.Blog(),
            "/blog/",
            os.path.join(self.conf_path, "app.cfg")
            )

        cherrypy.tree.mount(
            webapp.game.game.Game(),
            "/game/",
            os.path.join(self.conf_path, "app.cfg")
            )

        cherrypy.tree.mount(
            webapp.gallery.gallery.Gallery(),
            "/gallery/",
            os.path.join(self.conf_path, "app.cfg")
            )

        cherrypy.tree.mount(
            webapp.chat.chat.Chat(),
            "/chat/",
            os.path.join(self.conf_path, "chat.cfg")
            )

        cherrypy.tree.mount(
            webapp.admin.admin.AdminController(),
            "/admin/",
            os.path.join(self.conf_path, "app.cfg")
            )

    def run(self):
        engine = cherrypy.engine
        if hasattr(engine, "signal_handler"):
            engine.signal_handler.subscribe()

        if hasattr(engine, "console_control_handler"):
            engine.console_control_handler.subscribe()

        engine.start()
        engine.block()


if __name__ == "__main__":
    Server().run()
