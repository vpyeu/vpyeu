from datetime import datetime
import cherrypy
import sqlalchemy
from sqlalchemy import Column, Index, ForeignKey, UniqueConstraint
from sqlalchemy.types import Integer, String

from lib.model import ORBase

__all__ = ["Category"]


class Category(ORBase):
    __tablename__ = "categories"

    category_id = Column(Integer, nullable=False, primary_key=True)
    category_name = Column(String(30))

    UniqueConstraint(category_name)
    Index(category_id)
    Index(category_name)

    def __repr__(self):
        return self.category_name

    @classmethod
    def get_by(cls, attr_name="category_id", value=None):
        return cherrypy.request.db.query(cls).filter(
            object.__getattribute__(cls, attr_name) == value
            ).first()
