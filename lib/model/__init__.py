from sqlalchemy.ext.declarative import declarative_base

__all__ = ["ORBase"]

ORBase = declarative_base()
