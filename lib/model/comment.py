from datetime import datetime
import cherrypy
import sqlalchemy
from sqlalchemy import Column, Index, ForeignKey, desc
from sqlalchemy.types import Integer, String, DateTime

from lib.model import ORBase
import lib.model.user

__all__ = ["Comment"]


class Comment(ORBase):
    __tablename__ = "comments"

    comment_id = Column(Integer, nullable=False, primary_key=True)
    comment_type = Column(String(10), nullable=False)
    parent_id = Column(Integer, nullable=False)
    content = Column(String(140), nullable=False)
    created_by = Column(Integer, ForeignKey("users.user_id"))
    created_at = Column(DateTime)

    Index(comment_id)

    def __init__(self, comment_type="Post", parent_id=None,
                 content=None, created_by=None):
        self.comment_type = comment_type
        self.parent_id = parent_id
        self.content = content
        self.created_by = created_by
        self.created_at = datetime.utcnow()

    def __repr__(self):
        return self.content

    @classmethod
    def get_by(cls, attr_name="comment_id", value=None):
        return cherrypy.request.db.query(
            cls,
            lib.model.user.User
            ).join(lib.model.user.User).filter(
            object.__getattribute__(cls, attr_name) == value
            ).first()

    @classmethod
    def create(cls, comment_type="Post", parent_id=None, content=None, current_user=None):
        if (not current_user or
                not comment_type or
                not parent_id or
                not content):
            return False
        comment = cls(
            comment_type=comment_type,
            parent_id=parent_id,
            content=content,
            created_by=current_user.user_id
            )
        cherrypy.request.db.add(comment)
        cherrypy.request.db.commit()
        return True

    @classmethod
    def all(cls):
        return cherrypy.request.db.query(cls, lib.model.user.User).join(lib.model.user.User).order_by(
            cls.created_at.desc())
