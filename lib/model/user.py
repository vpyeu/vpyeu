import cherrypy
import sqlalchemy
from sqlalchemy import Column, UniqueConstraint, Index
from sqlalchemy.types import Integer, String, Boolean
import re

from lib.model import ORBase
from lib.plugin.hash import (
    encrypt_sha512,
    encrypt_md5,
    random_string_generator
    )
from conf.system_config import username_pattern, email_pattern
import lib.model.entry
import lib.model.post
from lib.tool import mail

__all__ = ["User"]


class User(ORBase):
    __tablename__ = "users"

    user_id = Column(Integer, nullable=False, primary_key=True)
    name = Column(String(20), nullable=False)
    fullname = Column(String(50))
    email = Column(String(50))
    password = Column(String(128))
    pr = Column(String(140))
    is_admin = Column(Boolean)
    activated = Column(Boolean)
    activate_key = Column(String(10))

    UniqueConstraint(name)
    Index(user_id)
    Index(name)

    def __init__(self, name=None, fullname=None, email=None, password=None,
                 pr=None):
        self.name = name
        self.fullname = fullname
        self.email = email
        self.password = encrypt_sha512(password)
        self.pr = pr
        self.is_admin = False
        self.activated = False
        self.activate_key = random_string_generator()

    def __repr__(self):
        return self.fullname or self.name

    def gravatar(self, size=50):
        encrypted_mail = encrypt_md5(self.email)
        return "https://secure.gravatar.com/avatar/%s?s=%s" % (
            encrypted_mail, size)

    def update(self, name=None, fullname=None, email=None, password=None,
               pr=None):
        self.name = name or self.name
        self.fullname = fullname or self.fullname
        if User.is_valid_attr("email", email, self.name):
            self.email = email or self.email
        if password:
            self.password = encrypt_sha512(password)
        self.pr = pr or self.pr
        cherrypy.request.db.add(self)
        cherrypy.request.db.commit()

    def generate_key(self):
        if self.activated:
            return False
        self.activate_key = random_string_generator()
        cherrypy.request.db.add(self)
        cherrypy.request.db.commit()
        mail.send_activate_key(
            self.email,
            self.fullname,
            self.activate_key
            )
        return True

    def activate(self, key=None):
        if self.activated:
            return "Account activated"
        if key != self.activate_key:
            return "Key mismatched"
        self.activated = True
        self.activate_key = None
        cherrypy.request.db.add(self)
        cherrypy.request.db.commit()

    def get_entries(self):
        return lib.model.entry.Entry.get_all_by("author", self.user_id)

    def get_posts(self):
        return lib.model.post.Post.get_all_by("created_by", self.user_id)

    @classmethod
    def get_all(cls, attr_name="user_id", value=None):
        return None if not value else cherrypy.request.db.query(cls).filter(
            object.__getattribute__(cls, attr_name) == value
            )

    @classmethod
    def get_by(cls, attr_name="user_id", value=None):
        return None if not value else cls.get_all(attr_name, value).first()

    @classmethod
    def create(cls, name=None, fullname=None, email=None, password=None,
               pr=None):
        if not name or not email or not password:
            return False
        if (not cls.is_valid_attr("name", name) or
                not cls.is_valid_attr("email", email)):
            return False
        new_user = cls(
            name=name,
            fullname=fullname,
            email=email,
            password=password, pr=pr
            )
        cherrypy.request.db.add(new_user)
        cherrypy.request.db.commit()
        mail.send_activate_key(
            new_user.email,
            new_user.fullname,
            new_user.activate_key
            )
        return True

    @classmethod
    def is_valid_attr(cls, attr_name="name", value=None, accept=None):
        if not value:
            return False if not accept else True
        if attr_name == "name" and not re.match(username_pattern, value):
            return False
        if attr_name == "email" and not re.match(email_pattern, value):
            return False
        user = cls.get_by(attr_name, value)
        if user:
            if not accept or user.name != accept:
                return False
        return True

    @classmethod
    def get_admin(cls):
        return cls.get_all("is_admin", True)
