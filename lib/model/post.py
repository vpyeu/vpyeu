from datetime import datetime
import cherrypy
import sqlalchemy
from sqlalchemy import Column, Index, ForeignKey, desc, and_
from sqlalchemy.types import Integer, String, DateTime

from lib.model import ORBase
import lib.model.user
from lib.model.comment import Comment

__all__ = ["Post"]


class Post(ORBase):
    __tablename__ = "posts"

    post_id = Column(Integer, nullable=False, primary_key=True)
    content = Column(String(140), nullable=False)
    created_by = Column(Integer, ForeignKey("users.user_id"))
    created_at = Column(DateTime)

    Index(post_id)

    def __init__(self, content=None, created_by=None):
        self.content = content
        self.created_by = created_by
        self.created_at = datetime.utcnow()

    def __repr__(self):
        return self.content

    def get_comments(self):
        return cherrypy.request.db.query(Comment, lib.model.user.User).join(
            lib.model.user.User).filter(
            and_(
                Comment.comment_type == "Post",
                Comment.parent_id == self.post_id
                )
            )

    @classmethod
    def get_all_by(cls, attr_name="post_id", value=None):
        return cherrypy.request.db.query(cls, lib.model.user.User).join(
            lib.model.user.User).filter(
            object.__getattribute__(cls, attr_name) == value
            )

    @classmethod
    def get_by(cls, attr_name="post_id", value=None):
        return cls.get_all_by(attr_name, value).first()

    @classmethod
    def create(cls, content=None, current_user=None):
        if not current_user:
            return False
        post = cls(content=content, created_by=current_user.user_id)
        cherrypy.request.db.add(post)
        cherrypy.request.db.commit()
        return True

    @classmethod
    def all(cls):
        return cherrypy.request.db.query(cls, lib.model.user.User).join(
            lib.model.user.User).order_by(
            cls.created_at.desc())
