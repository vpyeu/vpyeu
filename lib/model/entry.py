from datetime import datetime
import cherrypy
import sqlalchemy
from sqlalchemy import Column, Index, ForeignKey, Table, or_, desc
from sqlalchemy.orm import relationship
from sqlalchemy.types import Integer, String, DateTime, Text, Boolean

from lib.model import ORBase
from lib.model.page import Page
from lib.model.category import Category
import lib.model.user

__all__ = ["Entry"]

association_table = Table(
    'entry_categories', ORBase.metadata,
    Column('entry_id', Integer, ForeignKey('entries.entry_id')),
    Column('category_id', Integer, ForeignKey('categories.category_id'))
    )

class Entry(ORBase):
    __tablename__ = "entries"

    entry_id = Column(Integer, nullable=False, primary_key=True)
    page = Column(Integer, ForeignKey("pages.page_id"))
    categories = relationship("Category", secondary=association_table)
    headline = Column(String(255))
    body_text = Column(Text)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    published_at = Column(DateTime)
    author = Column(Integer, ForeignKey("users.user_id"))
    is_closed = Column(Boolean)

    def __init__(self, headline=None, body_text=None, page=None):
        now = datetime.utcnow()
        self.created_at = now
        self.updated_at = now
        self.published_at = None
        self.page = page
        self.author = 1
        self.headline = headline
        self.body_text = body_text
        self.page = page
        self.is_closed = False

    def __repr__(self):
        return self.headline

    def set_category(self, category=None):
        if category and category not in self.categories:
            self.categories.append(category)
            return True
        else:
            return False

    def remove_category(self, category=None):
        if category and category in self.categories:
            self.categories.delete(category)
            return True
        else:
            return False

    def update(self, headline=None, body_text=None, page=None):
        self.headline = headline
        self.body_text = body_text
        self.page = page
        self.updated_at = datetime.utcnow()
        cherrypy.request.db.add(self)
        cherrypy.request.db.commit()

    def publish(self):
        self.published_at = datetime.now()
        cherrypy.request.db.add(self)
        cherrypy.request.db.commit()

    def close(self):
        self.is_closed = True
        cherrypy.request.db.add(self)
        cherrypy.request.db.commit()

    @classmethod
    def create(cls, headline=None, body_text=None, page=None,
               categories = None):
        new_entry = cls(headline, body_text, page)
        if categories:
            for category_id in categories:
                category = Category.get_by("category_id", category_id)
                new_entry.set_category(category)
        cherrypy.request.db.add(new_entry)
        cherrypy.request.db.commit()
        return new_entry


    @classmethod
    def get_all_by(cls, attr_name="page_id", value=None):
        return cherrypy.request.db.query(cls, lib.model.user.User).join(lib.model.user.User).filter(
            object.__getattribute__(cls, attr_name) == value
            )

    @classmethod
    def get_by(cls, attr_name="page_id", value=None):
        return cls.get_all_by(attr_name, value).first()

    @classmethod
    def get_for(cls, user=None):
        all_entries = cherrypy.request.db.query(cls, lib.model.user.User).join(lib.model.user.User).filter(
            cls.published_at != None
            )
        if user:
            entries = all_entries.filter(
                or_(
                    cls.is_closed == False,
                    cls.author == user.user_id,
                    )
                )
        else:
            entries = all_entries.filter(
                cls.is_closed == False
                )
        return entries.order_by(cls.published_at.desc())
