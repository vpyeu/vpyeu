from datetime import datetime
import cherrypy
import sqlalchemy
from sqlalchemy import Column, Index, ForeignKey, desc, and_
from sqlalchemy.types import Integer, String, DateTime, Boolean

from lib.model import ORBase

__all__ = ["AdminMessage"]


class AdminMessage(ORBase):
    __tablename__ = "admin_messages"

    message_id = Column(Integer, nullable=False, primary_key=True)
    content = Column(String(140), nullable=False)
    sender = Column(String(140))
    sender_email = Column(String(140))
    sent_at = Column(DateTime)
    unread = Column(Boolean)

    Index(message_id)

    def __init__(self, content=None, sender=None, sender_email=None):
        self.content = content
        self.sender = sender
        self.sender_email = sender_email
        self.sent_at = datetime.utcnow()
        self.unread = True

    def __repr__(self):
        return self.content

    def set_read(self):
        self.unread = False
        cherrypy.request.db.add(self)
        cherrypy.request.db.commit()

    @classmethod
    def create(cls, content=None, sender=None, sender_email=None):
        if not sender or not sender_email:
            return False
        new_msg = cls(
            content=content,
            sender=sender,
            sender_email=sender_email
            )
        cherrypy.request.db.add(new_msg)
        cherrypy.request.db.commit()
        return True

    @classmethod
    def all(cls):
        return cherrypy.request.db.query(cls).order_by(
            cls.sent_at.desc())

    @classmethod
    def all_unread(cls):
        return cls.all().filter(cls.unread == True)

    @classmethod
    def get_all_by(cls, attr_name="message_id", value=None):
        return cherrypy.request.db.query(cls).filter(
            object.__getattribute__(cls, attr_name) == value
            )

    @classmethod
    def get_by(cls, attr_name="message_id", value=None):
        return cls.get_all_by(attr_name, value).first()
