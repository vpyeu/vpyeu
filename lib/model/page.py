from datetime import datetime
import cherrypy
import sqlalchemy
from sqlalchemy import Column, Index, UniqueConstraint
from sqlalchemy.types import Integer, String

from lib.model import ORBase

__all__ = ["Page"]


class Page(ORBase):
    __tablename__ = "pages"

    page_id = Column(Integer, nullable=False, primary_key=True)
    page_name = Column(String(30))

    UniqueConstraint(page_name)
    Index(page_id)
    Index(page_name)

    def __repr__(self):
        return self.page_name

    @classmethod
    def get_by(cls, attr_name="page_id", value=None):
        return cherrypy.request.db.query(cls).filter(
            object.__getattribute__(cls, attr_name) == value
            ).first()
