import cherrypy
import mako.template
import mako.lookup
import string
import random

from lib.model.user import User
from conf.system_config import SESSION_KEY, tmpl_lookup

def get_current_user():
    username = cherrypy.session.get(SESSION_KEY)
    return User.get_by("name", username)

def handle_404_error(status, message, traceback, version):
    tmpl = mako.template.Template(
        filename="template/error/404.html",
        lookup=tmpl_lookup
        )
    return tmpl.render(
        current_user=get_current_user(),
        status=status,
        msg=message
        )
def is_xhr():
    requested_with = cherrypy.request.headers.get('X-Requested-With')
    return requested_with and requested_with.lower() == 'xmlhttprequest'
