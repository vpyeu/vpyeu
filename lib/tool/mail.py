#!/usr/bin/env python3

import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from conf.system_config import mail_config, admin_mail_from


def admin_send_answer(receiver, receiver_name, content, origin=None):
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "Feedback from administrator - Viet Phuong yeu"
    msg['From'] = admin_mail_from
    msg['To'] = receiver
    msg['Date'] = formatdate()

    text = "Hi, {name}!\n{content}\n\nYou have written\n{origin}".format(
        name=receiver_name,
        content=content,
        origin=origin
        )
    html = """\
<html>
  <head></head>
  <body>
    <p>Hi, {name}!<br>
      {content}<br><br>
      You have written<br>
      {origin}
    </p>
  </body>
</html>
""".format(
        name=receiver_name,
        content=content,
        origin=origin
        )

    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')
    msg.attach(part1)
    msg.attach(part2)

    s = smtplib.SMTP(mail_config["host"], 25)
    if "username" in mail_config and "password" in mail_config:
        s.login(mail_config["username"], mail_config["password"])
    s.sendmail(admin_mail_from, receiver, msg.as_string())
    s.quit()

def send_activate_key(receiver, receiver_name, key):
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "Activation information - Viet Phuong yeu"
    msg['From'] = admin_mail_from
    msg['To'] = receiver
    msg['Date'] = formatdate()

    activate_link = "http://vpyeu.local/activate?email=%s&key=%s" % (
        receiver, key)
    text = "Hi, {name}!\nPlease activate your account with by click the link below\n{link}".format(
        name=receiver_name,
        link=activate_link
        )
    html = """\
<html>
  <head></head>
  <body>
    <p>Hi, {name}!<br>
      Please activate your account by click the link below<br>
      <a href="{link}">{link}</a>
    </p>
  </body>
</html>
""".format(
        name=receiver_name,
        link=activate_link
        )

    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')
    msg.attach(part1)
    msg.attach(part2)

    s = smtplib.SMTP(mail_config["host"], 25)
    if "username" in mail_config and "password" in mail_config:
        s.login(mail_config["username"], mail_config["password"])
    s.sendmail(admin_mail_from, receiver, msg.as_string())
    s.quit()
