import cherrypy
from lib.model.user import User
from lib.plugin.hash import encrypt_sha512
from conf.system_config import SESSION_KEY

from lib.tool import web

def check_credentials(username, password):
    """Verifies credentials for username and password.
    Returns None on success or messages of error"""
    user = User.get_by("name", username)
    if not user:
        return "Incorrect username"
    if user.password != encrypt_sha512(password):
        return "Incorrect password"
    if not user.activated:
        return "Account is not activated"

def check_auth(*args, **kwargs):
    """A tool that looks in config for 'auth.require'. If found and it
    is not None, a login is required and the entry is evaluated as a list of
    conditions that the user must fulfill"""
    conditions = cherrypy.request.config.get('auth.require', None)
    if conditions is not None:
        username = cherrypy.session.get(SESSION_KEY)
        if username:
            cherrypy.request.login = username
            for condition in conditions:
                # A condition is just a callable that returns true or false
                if not condition():
                    if web.is_xhr():
                        raise cherrypy.HTTPRedirect("/auth/login")
                    raise cherrypy.HTTPRedirect("/")
        else:
            if web.is_xhr():
                raise cherrypy.HTTPRedirect("/auth/login")
            raise cherrypy.HTTPRedirect("/")

cherrypy.tools.auth = cherrypy.Tool('before_handler', check_auth)

def require(*conditions):
    """A decorator that appends conditions to the auth.require config
    variable."""
    def decorate(f):
        if not hasattr(f, '_cp_config'):
            f._cp_config = dict()
        if 'auth.require' not in f._cp_config:
            f._cp_config['auth.require'] = []
        f._cp_config['auth.require'].extend(conditions)
        return f
    return decorate


# Conditions are callables that return True
# if the user fulfills the conditions they define, False otherwise
#
# They can access the current username as cherrypy.request.login
#
# Define those at will however suits the application.

def member_of(groupname):
    def check():
        return cherrypy.request.login in [groupname]
    return check

def name_is(reqd_username):
    return lambda: reqd_username == cherrypy.request.login

def is_admin():
    def check():
        username = cherrypy.request.login
        user = User.get_by("name", username)
        return user.is_admin
    return check

# These might be handy

def any_of(*conditions):
    """Returns True if any of the conditions match"""
    def check():
        for c in conditions:
            if c():
                return True
        return False
    return check

# By default all conditions are required, but this might still be
# needed if you want to use it inside of an any_of(...) condition
def all_of(*conditions):
    """Returns True if all of the conditions match"""
    def check():
        for c in conditions:
            if not c():
                return False
        return True
    return check
