import hashlib
import string
import random

__all__ = ["encrypt_sha512", "encrypt_md5", "random_string_generator"]

def encrypt_sha512(s):
    m = hashlib.sha512()
    m.update(s.encode("utf-8"))
    return m.hexdigest()

def encrypt_md5(s):
    m = hashlib.md5()
    m.update(s.encode("utf-8"))
    return m.hexdigest()


def random_string_generator(size=10,
                            chars=string.ascii_uppercase + string.digits):
    return "".join(random.choice(chars) for _ in range(size))
