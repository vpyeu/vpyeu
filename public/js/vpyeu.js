$(document).ready(function() {
    var MQL = 1170;

    //primary navigation slide-in effect
    if ($(window).width() > MQL) {
        var headerHeight = $('.navbar-custom').height();
        $(window).on('scroll', {
            previousTop: 0
        }, function() {
            var currentTop = $(window).scrollTop();
            //check if user is scrolling up
            if (currentTop < this.previousTop) {
                //if scrolling up...
                if (currentTop > 0 && $('.navbar-custom').hasClass('is-fixed')) {
                    $('.navbar-custom').addClass('is-visible');
                } else {
                    $('.navbar-custom').removeClass('is-visible is-fixed');
                }
            } else {
                //if scrolling down...
                $('.navbar-custom').removeClass('is-visible');
                if (currentTop > headerHeight && !$('.navbar-custom').hasClass('is-fixed'))
                    $('.navbar-custom').addClass('is-fixed');
            }
            this.previousTop = currentTop;
        });
    }

    var offset = 220;
    var duration = 500;
    vh = $(window).height();
    vw = $(window).width();
    vmin = Math.min(vh, vw);
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > offset) {
            jQuery('.back-to-top').fadeIn(duration);
        } else {
            jQuery('.back-to-top').fadeOut(duration);
        }

        if ($(this).scrollTop() < vmin) {
            set_carousel_scroll();
        }
    });

    jQuery('.back-to-top').click(function(event) {
        event.preventDefault();
        jQuery('html, body').animate({
            scrollTop: 0
        }, duration);
        return false;
    })


    $('#top-login-btn').click(function() {
        do_login();
    });

    $('#top-login-form').each(function() {
        $(this).find('input').keypress(function(e) {
            if (e.which == 10 || e.which == 13) {
                do_login();
            }
        });
    });

    $('#top-logout-btn').click(function() {
        if (confirm("You will be logged out!")) {
            $.ajax({
                type: "GET",
                url: "/auth/logout",
                success: function() {
                    location.reload();
                },
                error: function() {
                    alert("Unknown error occurs");
                    location.reload();
                }
            });
        }
    });

    set_carousel_position();

    $('.carousel').carousel({
        interval: 4000
    })

    if (is_smartphone()) {
        $("audio").attr("controls", "controls");
    }
});

$(window).on("resize", function() {
    set_carousel_position();
});

function set_carousel_position() {
    $(".carousel img").each(function() {
        height = this.naturalHeight;
        width = this.naturalWidth;
        scale_h = $(".carousel-inner").height() / height;
        scale_w = $(".carousel-inner").width() / width;
        if (scale_h < scale_w) {
            height = height * scale_w;
            $(this).css("top", ($(".carousel-inner").height() - height)/2);
        }
    });
}

function set_carousel_scroll() {
    $(".carousel img").each(function() {
        height = this.naturalHeight;
        width = this.naturalWidth;
        s_height = $(".carousel-inner").height() - $(window).scrollTop();
        c_height = $(".carousel-inner").height() + $(window).scrollTop();
        s_width = $(".carousel-inner").width();
        scale_h = s_height / height;
        scale_w = s_width / width;
        if (scale_h < scale_w) {
            height = height * scale_w;
            offset_top = (c_height - height) / 2;
            if ($(window).width() <  768) {
                offset_top = offset_top - 70;
            }
            $(this).css("top", offset_top);
        } else {
            $(this).css("top", "");
        }
    });
}

function do_login() {
    username = $('#username-top').val();
    password = $('#password-top').val();
    $.ajax({
        type: "POST",
        url: "/auth/login",
        data: {
            "username": username,
            "password": password
        },
        dataType: "json",
        success: function(data) {
            if (data.status != "success") {
                alert(data.status);
                return;
            }
            window.location.href = window.location.href.split('#')[0].split("?")[0];
        },
        error: function() {
            alert("Unknown error occurs");
        }
    });
}

function is_smartphone() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
}