$(document).ready(function() {
    var url = window.location.pathname;
    $('.nav-tabs li a').each(function() {
        var hrefPage = $(this).attr('href');
        if (hrefPage.split("/")[2] == url.split("/")[2]) {
            $(this).parent().addClass('active');
        }
    });



    $('#entry-save').click(function() {
        $('[id^=entry]').attr('disabled', 'disabled');
        toggleReadOnly(true);
        entry_id = $('#entry-id').val()
        headline = $('#entry-headline').val()
        body_text = getData();
        $.ajax({
            type: "POST",
            url: "/blog/entry/j_save",
            data: {
                "entry_id": entry_id,
                "headline": headline,
                "body_text": body_text
            },
            dataType: "json",
            success: function(data) {
                alert(data.status);
                window.location.href = "/blog/entry/write/" + data.entry_id;
            },
            error: function() {
                alert("Unknown error occurs");
            }
        });
        $('[id^=entry]').removeAttr('readonly');
        toggleReadOnly(false);
    });

    $('#entry-publish').click(function() {
        $('[id^=entry-]').attr('disabled', 'disabled');
        toggleReadOnly(true);
        entry_id = $('#entry-id').val()
        headline = $('#entry-headline').val()
        body_text = getData();
        $.ajax({
            type: "POST",
            url: "/blog/entry/j_publish",
            data: {
                "entry_id": entry_id,
                "headline": headline,
                "body_text": body_text
            },
            dataType: "json",
            success: function(data) {
                alert(data.status);
                window.location.href = "/blog/entry/view/" + data.entry_id;
            },
            error: function() {
                alert("Unknown error occurs");
            }
        });
        $('[id^=entry-]').removeAttr('readonly');
        toggleReadOnly(false);
    });


});


function toggleReadOnly(isReadOnly) {
    var editor = CKEDITOR.instances["entry-body"];
    editor.setReadOnly(isReadOnly);
}

function getData() {
    var editor = CKEDITOR.instances["entry-body"];
    return editor.getData();
}