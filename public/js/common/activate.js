$(document).ready(function() {
    $("#resend-key-form").hide();

    $("#resend-key-btn").click(function() {
        $("#resend-key-form").show();
    });

    $("#resend-key-form-btn").click(function() {
        do_resend_key();
    });
    $("#resend-key-form #email").keyup(function(e) {
        if (e.which == 10 || e.which == 13) {
            e.preventDefault();
            do_resend_key();
        }
    });
});

function do_resend_key() {
    email = $("#resend-key-form #email").val();
    $("#resend-key-form #email").attr("readonly", "readonly");
    $("#resend-key-form-btn").attr("disable", "disable");
    $("#resend-key-form-btn").html("<span class='glyphicon glyphicon-send'></span> Sending");
    $.ajax({
        type: "POST",
        url: "/j_resend_key",
        data: {email: email},
        dataType: "json",
        success: function(data) {
            if (data.status == "success") {
                alert("An email is sent to your account");
                location.reload();
                restore_resend_state();
                $("#resend-key-form #email").val("");
            } else {
                alert("Error");
                restore_resend_state();
            }
        },
        error: function() {
            alert("Unknown error occurs");
            restore_resend_state();
        }
    });
}

function restore_resend_state() {
    $("#resend-key-form #email").removeAttr("readonly");
    $("#resend-key-form-btn").removeAttr("disabled");
    $("#resend-key-form-btn").html("<span class='glyphicon glyphicon-send'></span> Resend");
}