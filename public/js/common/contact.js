$(document).ready(function() {
    $("#contact-btn").click(function() {
        sender = $("#contact-name").val();
        sender_email = $("#contact-email").val();
        msg = $("#contact-msg").val();
        if (!sender || !sender_email || !msg) {
            return
        }
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!regex.test(sender_email)) {
            alert("Enter a valid email address!");
            return;
        }
        $("[id^=contact-]").attr("disabled", "disabled");
        $("#contact-btn").html("<span class='glyphicon glyphicon-send'></span> Sending");
        $.ajax({
            type: "POST",
            url: "/j_contact",
            data: {sender: sender, sender_email: sender_email, content: msg},
            success: function(data) {
                if (data.status == "success") {
                    alert("Your message have been sent");
                    restore_contact_state();
                    location.reload();
                    $("[id^=contact-]").val("");
                } else {
                    alert("Error");
                    restore_contact_state();
                }
            },
            error: function() {
                alert("Unknown error occurs");
                restore_contact_state();
            }
        });
    });
});

function restore_contact_state() {
    $("[id^=contact-]").removeAttr("disabled");
}