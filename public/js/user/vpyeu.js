$(document).ready(function() {
    g_valid_name = false;
    g_valid_email = false;
    alert_ok = "<span class='glyphicon glyphicon-ok'></span>"
    alert_ng = "<span class='glyphicon glyphicon-remove'></span>"
    $('#reg-name').blur(function() {
        var name = $('#reg-name').val()
        $.ajax({
            type: "POST",
            url: "/user/j_is_valid",
            data: {
                "attr_name": "name",
                "value": name
            },
            dataType: "json",
            success: function(data) {
                if (data.status == "valid") {
                    $("#reg-name-alert").html(alert_ok);
                    g_valid_name = true;
                    if (g_valid_name && g_valid_email) {
                        $('#reg-btn').removeAttr('disabled')
                    };
                } else {
                    g_valid_name = false;
                    $("#reg-name-alert").html(alert_ng);
                    $('#reg-btn').attr('disabled', 'disabled');
                }
            },
        });
    });

    $('#reg-email').blur(function() {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var email = $('#reg-email').val()
        if (!regex.test(email)) {
            alert("Enter a valid email address!");
            return;
        }
        $.ajax({
            type: "POST",
            url: "/user/j_is_valid",
            data: {
                "attr_name": "email",
                "value": email
            },
            dataType: "json",
            success: function(data) {
                if (data.status == "valid") {
                    g_valid_email = true;
                    $('#reg-email-alert').html(alert_ok);
                    if (g_valid_name && g_valid_email) {
                        $('#reg-btn').removeAttr('disabled')
                    };
                } else {
                    g_valid_email = false;
                    $("#reg-email-alert").html(alert_ng);
                    $('#reg-btn').attr('disabled', 'disabled');
                }
            },
        });
    });


    $('#edit-email').blur(function() {
        var email = $('#edit-email').val();
        var name = $("#name").val();
        $.ajax({
            type: "POST",
            url: "/user/j_is_valid",
            data: {
                "attr_name": "email",
                "value": email,
                "accept": name
            },
            dataType: "json",
            success: function(data) {
                if (data.status == "valid") {
                    $('#edit-btn').removeAttr('disabled')
                } else {
                    alert("Email already exist");
                    $('#edit-btn').attr('disabled', 'disabled')
                }
            }
        });
    });
});