$(document).ready(function() {
    $('#post-send').click(function() {
        $('#post-content').attr('readonly', 'readonly');
        $('#post-send').hide();
        $('#post-sending').addClass('btn btn-default').show();
        var msg = $('#post-content').val();
        if (!msg.trim()) {
            alert("Your post is empty!");
            restore_post_state();
            return;
        }
        if (msg.trim().length > 140) {
            alert("Your post is too long!");
            restore_post_state();
            return;
        }
        $.ajax({
            type: "POST",
            url: "/sns/post/j_create",
            data: {
                content: msg
            },
            dataType: "json",
            success: function(data) {
                if (data.status == "success") {
                    location.reload();
                    $('#post-content').val("");
                } else {
                    alert("Error");
                    restore_post_state();
                }
            },
            error: function() {
                alert("Unknown error occurs");
                restore_post_state();
            }
        });
    });

    $('.comment').click(function(event) {
        event.preventDefault();
        post_id = this.id;
        $.ajax({
            type: "GET",
            url: "/sns/comment/comment",
            data: {
                "post_id": post_id
            },
            dataType: "html",
            success: function(html) {
                selector = '#comment' + post_id;
                $(selector).html(html);
                $("#comment-content-" + post_id).focus();
            }
        });
    });

    $('#post-content').keyup(function() {
        var cs = $('#post-content').val().length;
        $('#char-count').html(140 - cs);
        if (cs > 140) {
            $('#post-send').attr('disabled', 'disabled');
        } else {
            $('#post-send').removeAttr('disabled');
        }
    });
});

function restore_post_state() {
    $('#content').removeAttr('readonly');
    $('#post-send').show();
    $('#post-sending').hide();
}

function do_comment(s) {
    post_id = parseInt(s.id.substr(12));
    content_selector = "#comment-content-" + post_id;
    content = $(content_selector).val()
    $(content_selector).attr("readonly", "readonly");

    if (!content.trim()) {
        alert("Your comment is empty!");
        restore_comment_state(post_id);
        return;
    }

    commenting_sl = "#commenting-" + post_id;
    comment_sl = "#comment-btn-" + post_id;
    $(commenting_sl).addClass("btn btn-info pull-right").show();
    $(comment_sl).hide();
    $.ajax({
        type: "POST",
        url: "/sns/comment/comment",
        data: {
            "post_id": post_id,
            "content": content
        },
        dataType: "json",
        success: function(data) {
            location.reload();
            $(content_selector).val("");
        },
        error: function() {
            alert("Unknown error occurs");
            restore_comment_state(post_id);
        }
    });
}

function restore_comment_state(post_id) {
    content_selector = "#comment-content-" + post_id;
    $(content_selector).removeAttr("readonly");
    commenting_sl = "#commenting-" + post_id;
    comment_sl = "#comment-btn-" + post_id;
    $(commenting_sl).hide();
    $(comment_sl).show();
}

function comment_char_count(post_id) {
    var msg = $("#comment-content-" + post_id).val().trim();
    $("#comment-" + post_id + "-cnt").html(140 - msg.length);
    if (msg.length > 140) {
        $("#comment-btn-" + post_id).attr("disabled", "disabled");
    } else {
        $("#comment-btn-" + post_id).removeAttr("disabled");
    }
}