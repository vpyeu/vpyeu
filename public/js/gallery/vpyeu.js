$(document).ready(function() {
    $("#bottom-btn").click(function() {
        $("html, body").animate({
            scrollTop: $(document).height()
        }, "slow");
        return false;
    });

    $("#upload-form").hide();
    var g_form_open = false;
    $("#upload-btn").click(function() {
        if (!g_form_open) {
            $("#upload-form").show();
            $("#upload-btn").html("<span class='glyphicon glyphicon-remove'></span> Close");
            g_form_open = true;
        } else {
            $("#upload-form").hide();
            $("#upload-btn").html("<span class='glyphicon glyphicon-upload'></span> Upload");
            g_form_open = false;
        }
    });
});