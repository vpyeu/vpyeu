$(document).ready(function() {
    $("#feedback-btn").click(function() {
        $("[id^=feedback]").attr("disabled", "disabled");
        message_id = $("#feedback-message-id").val();
        content = $("#feedback-msg").val();

        $.ajax({
            type: "POST",
            url: "/admin/message/j_feedback",
            data: {message_id: message_id, content: content},
            dataType: "json",
            success: function(data) {
                if (data.status == "success") {
                    alert("Your feedback has been sent");
                    restore_state_feedback();
                    location.reload();
                    $("#feedback-msg").val("");
                } else {
                    alert("Error");
                    restore_state_feedback();
                }
            },
            error: function() {
                alert("Unknown error occurs");
                restore_state_feedback();
            }
        });
    });
});

function restore_state_feedback() {
    $("[id^=feedback]").removeAttr("disabled");
}